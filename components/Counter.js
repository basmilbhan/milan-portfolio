import React from 'react';

export const Counter = () => {
  const counts = [
    {
      icon: 'interface',
      count: 258,
      title: 'Happy Clients',
    },
    {
      icon: 'law-protect',
      count: 590,
      title: 'Project Complete',
    },
    {
      icon: 'ui-calendar',
      count: 25,
      title: 'Years of Experience',
    },
  ];
  return (
    <div className=' bg-[#282a37] p-0 m-0'>
      <div className='max-w-[98%] mx-auto px-4 sm:px-6 lg:px-8 py-56 flex gap-12'>
        <div class='w-full'>
          <div className='flex justify-between w-full'>
            {counts.map((count) => (
              <div key={count} className='flex gap-20 items-center'>
                <div className='icon w-[70px] h-[70px] border flex items-center justify-center mr-0 rounded-[15px] border-solid border-[#40424d] '>
                  <i
                    className={`icofont-${count['icon']} text-6xl text-white `}
                  ></i>
                </div>
                <div className='flex flex-col gap-10'>
                  <h2 className='m-0'>
                    <span className='counter'>{count['count']}</span>+
                  </h2>
                  <p className='text text-2xl'>{count['title']}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};
