import { useEffect, useState } from 'react';
import Link from 'next/link';

export const Nav = () => {
  const [isScrolled, setIsScrolled] = useState(false);
  let menu_items = ['Home', 'Service', 'Blog', 'Contact'];

  useEffect(() => {
    function handleScroll() {
      const scrollPosition = window.scrollY;
      setIsScrolled(scrollPosition > 100);
    }

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);
  return (
    <nav
      className={`text-2xl ${
        isScrolled ? 'bg-[#252734]' : 'bg-transparent'
      } transition-all duration-300 py-2 px-4 fixed w-full z-20 top-0 left-0 `}
    >
      <div className='lg:max-w-[1440px] md:max-w-[744px] max-w-[375px] relative lg:px-10 md:px-6 px-4 py-7 flex flex-wrap items-center justify-between mx-auto p-4'>
        <Link href='/' className='flex items-center'>
          <span className='self-center text-6xl text-white font-semibold whitespace-nowrap '>
            Milan
          </span>
        </Link>
        <div className='flex md:order-2'>
          <button type='button' className='h-24 py-2  mr-3 md:mr-0'>
            Hire Me
            <i className='icofont-double-right'></i>
          </button>
          <button
            data-collapse-toggle='navbar-sticky'
            type='button'
            className='inline-flex items-center p-2 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200'
            aria-controls='navbar-sticky'
            aria-expanded='false'
          >
            <span className='sr-only'>Open main menu</span>
            <svg
              className='w-6 h-6'
              aria-hidden='true'
              fill='currentColor'
              viewBox='0 0 20 20'
              xmlns='http://www.w3.org/2000/svg'
            >
              <path
                fillRule='evenodd'
                d='M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z'
                clipRule='evenodd'
              />
            </svg>
          </button>
        </div>
        <div
          className='items-center justify-between hidden w-full md:flex md:w-auto md:order-1'
          id='navbar-sticky'
        >
          <ul className='flex flex-col p-4 gap-4 md:p-0 mt-4 font-medium md:flex-row md:space-x-8 md:mt-0'>
            {menu_items.map((item, key) => (
              <li key={key}>
                <a
                  href='#'
                  className='relative block py-2 pl-3 pr-4 text-3xl text-[#858792] rounded transition-all duration-[0.5s] ease-[ease] hover:after:visible hover:after:opacity-100  md:hover:bg-transparent md:hover:text-white md:p-0 '
                >
                  {item}
                </a>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </nav>
  );
};
