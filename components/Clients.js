import React from 'react';
import SliderComponent from './Slider';

export const Clients = () => {
  const clients = [
    'https://htmldemo.net/lendex/lendex/assets/images/company-logo/3.png',
    'https://htmldemo.net/lendex/lendex/assets/images/company-logo/2.png',
    'https://htmldemo.net/lendex/lendex/assets/images/company-logo/3.png',
    'https://htmldemo.net/lendex/lendex/assets/images/company-logo/2.png',
    'https://htmldemo.net/lendex/lendex/assets/images/company-logo/3.png',
    'https://htmldemo.net/lendex/lendex/assets/images/company-logo/2.png',
  ];

  const Slides = clients.map((client, key) => (
    <div
      key={key}
      className='clients border flex items-center justify-center  border-solid border-[#353743]'
    >
      <a
        href='#'
        className='flex items-center justify-center w-[14.2rem] h-[14.2rem] text-center relative'
      >
        <img
          className='opacity-[1] w-[14.2rem] h-[14.2rem]'
          src={client}
          alt=''
        />
        <img
          className='absolute w-[14.2rem] h-[14.2rem] top-0 left-0 opacity-0'
          src={client}
          alt=''
        />
      </a>
    </div>
  ));

  return (
    <div className='bg-[#252734]'>
      <div className=' max-w-[98%] mx-auto px-4 sm:px-6 lg:px-8 mt-8 py-56'>
        <div className='section-content mb-24'>
          <span className='section-tag before:absolute before:top-0 before:left-0 before:content-["||"] text-[1.8rem] font-light relative inline-block pl-11'>
            Favourite Clients
          </span>
          <h2 className='section-title'>Work With Trusted Comapny.</h2>
        </div>
        <SliderComponent
          slides={Slides}
          arrow={false}
          dot={false}
          slidePerPage={4}
        />
      </div>
    </div>
  );
};
