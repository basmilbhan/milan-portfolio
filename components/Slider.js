import React, { useState, useEffect } from 'react';
import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

const SliderComponent = (props) => {
  const [currentSlide, setCurrentSlide] = useState(0);

  const sliderSettings = {
    dots: props.dot,
    infinite: true,
    speed: 500,
    slidesToShow: props.slidePerPage,
    slidesToScroll: 1,
    initialSlide: 0,
    afterChange: (index) => setCurrentSlide(index),
    appendDots: (dots) => (
      <div className='flex justify-center -mb-4'>
        <ul className='flex justify-center'>{dots}</ul>
      </div>
    ),
    customPaging: () => (
      <div className='slick-dot w-2 h-2 mx-2 bg-gray-300 rounded-full'></div>
    ),
    arrows: props.arrow, // set arrows to false
    swipeToSlide: true,
  };

  return (
    <div className='relative'>
      {' '}
      <Slider {...sliderSettings}>{props.slides}</Slider>
    </div>
  );
};

export default SliderComponent;
