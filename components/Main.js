import React from 'react';
import { Nav } from './Nav';
import { Banner } from './Banner';
import { Services } from './Services';
import { Projects } from './Projects';
import { Skills } from './Skills';
import { Counter } from './Counter';
import { Clients } from './Clients';
import { Footer } from './Footer';

export const Main = () => {
  return (
    <>
      <Nav />
      <Banner />
      <Services />
      <Skills />
      <Counter />
      <Projects />
      <Clients />
      <Footer />
    </>
  );
};
