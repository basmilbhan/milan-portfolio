import React from 'react';

const HeroShape = (props) => {
  return (
    <div className='absolute z-[0] top-[-100px] translate-x-[-50%]'>
      <div
        className={`h-${props.mn} w-${props.mn} border rounded-full before:h-${props.bf} before:w-${props.bf}`}
      ></div>
    </div>
  );
};

export const Banner = () => {
  const socialLinks = ['facebook', 'instagram', 'linkedin', 'twitter'];
  return (
    <div className='py-12 px-4 bg-[#252734] opacity-100 h-[130vh] relative overflow-hidden'>
      <div className='z-[1] lg:max-w-[1440px] md:max-w-[744px] max-w-[375px] mx-auto  relative lg:px-10 md:px-6 px-4 py-7'>
        <div className='lg:max-w-[1280px] md:max-w-[696px] max-w-[343px] mx-auto  '>
          <div className='lg:flex md:flex block justify-between items-center'>
            <div className='translate-y-[25vh] flex flex-col gap-12 justify-start items-start'>
              <div>
                <h2>
                  Hi I&apos;m <br />
                </h2>
                <h1 className='py-4'>
                  Milan{' '}
                  <span className='relative inline-block after:inline-block after:h-full after:w-full after:absolute  after:bg-no-repeat after:bg-contain after:mx-3 after:left-[50%] after:translate-x-[-50%] after:bottom-[-75%] after:bg-[url(/hero-name-mark-shape.webp)]'>
                    Bhandari
                  </span>
                </h1>
              </div>
              <p className='leading-normal text-gray-600 w-10/12'>
                UI/UX Designer specializing in Shopify & Webflow.
              </p>
              <button>
                Get Resume
                <i class='icofont-download'></i>
              </button>

              <div class='flex gap-8 items-center xl:mt-10 lg:mt-8'>
                <a
                  class='flex  relative w-[9.6rem] h-[9.6rem] bg-gray-800 items-center justify-center no-underline outline-none shadow-none rounded-full'
                  href='https://youtu.be/MKjhBO2xQzg'
                  data-autoplay='true'
                  data-vbtype='video'
                >
                  <div class='ripple absolute w-full h-full flex items-center justify-center'>
                    <i class='icofont-ui-play text-2xl '></i>
                  </div>
                </a>
                <span class='text-2xl '> Watch Video</span>
              </div>
            </div>
            <div className='absolute top-[-30%] right-0 translate-x-[28%]'>
              <img src='/portrait-hero.webp' className='w-[70%] h-full z-0' />
              <div className='absolute z-[-1] rotate-[-25deg] top-[20%] translate-x-[-70%] left-[50%] translate-y-[13%]'>
                <div
                  className={`flex opacity-50 justify-center items-center  h-[700px] w-[700px] border-0 border-white  border-r  rounded-[50%] before:border-0 before:border-r  before:opacity-20 before:border-grey-700 before:block before:rounded-[50%] before:h-[580px] before:w-[580px] before:absolute after:opacity-20 after:border-grey-700 after:absolute after:border-0 after:border-r  after:block after:rounded-[50%] after:z-[-1] after:h-[820px] after:w-[820px]`}
                ></div>
              </div>

              <div className='absolute  translate-x-[50%] flex h-[600px] w-[300px] left-[35%] top-[26%] rotate-[-15deg] p-8 items-center content-center justify-between flex-col mb-4'>
                {socialLinks.map((social, key) => (
                  <a
                    key={key}
                    href='https://www.example.com'
                    target='_blank'
                    className={` ${
                      key > 0 && key < 3 ? 'self-end' : 'self-center'
                    } bg-gray-800 rotate-[15deg]  border-white border text-white flex justify-center items-center  rounded-full p-2 transition-colors duration-300 hover:bg-white h-24 w-24 hover:text-gray-800`}
                  >
                    <i className={`icofont-${social} text-3xl`}></i>
                  </a>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className='absolute z-[0] top-[0] translate-x-[-50%] translate-y-[-60%]'>
        <div
          className={`flex justify-center items-center opacity-10 h-[550px] w-[550px] border rounded-full before:border before:block before:rounded-full before:h-[400px] before:w-[400px] before:absolute  after:absolute after:border after:block after:rounded-full after:h-[700px] after:w-[700px]`}
        ></div>
      </div>
      <div className='absolute z-[0] bottom-[0] translate-x-[-50%] translate-y-[60%] '>
        <div
          className={`flex justify-center items-center opacity-10 h-[550px] w-[550px] border rounded-full before:border before:block before:rounded-full before:h-[400px] before:w-[400px] before:absolute  after:absolute after:border after:block after:rounded-full after:h-[700px] after:w-[700px]`}
        ></div>
      </div>
    </div>
  );
};
