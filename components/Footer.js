import React from 'react';

export const Footer = () => {
  const socialLinks = ['facebook', 'instagram', 'linkedin', 'twitter'];

  return (
    <footer className='relative overflow-hidden'>
      <div className='lg:py-[10rem] py-[7.5rem]'>
        <div className='w-full mx-auto max-w-[95%]'>
          <div className='mb-[-2rem] flex flex-wrap items-center justify-between '>
            <div className='mb-5 footer-single-info'>
              <a
                href='tel:+0123456789'
                className='info-box flex  gap-8 hover:text-white items-center'
              >
                <span className='icon xl:w-[8.5rem] xl:h-[8.5rem] lg:w-[6.5rem] lg:h-[6.5] md:w-[5.5rem] md:h-[5.5rem] h-[4.5rem] w-[4.5rem] border inline-flex items-center justify-center text-3xl text-white transition-all duration-[0.5s] ease-[ease] mr-5 rounded-[50%] border-solid border-[rgba(255,255,255,0.1)]'>
                  <i className='icofont-phone'></i>
                </span>
                <span className='xl:text-5xl lg:text-4xl md:text-3xl font-light'>
                  0123456789
                </span>
              </a>
            </div>
            <div className=' mb-5 footer-single-info'>
              <a
                href='mailto:demo@example.com'
                className='info-box flex  gap-8 hover:text-white items-center'
              >
                <span className='icon xl:w-[8.5rem] xl:h-[8.5rem] lg:w-[6.5rem] lg:h-[6.5] md:w-[5.5rem] md:h-[5.5rem] h-[4.5rem] w-[4.5rem] border inline-flex items-center justify-center text-3xl text-white transition-all duration-[0.5s] ease-[ease] mr-5 rounded-[50%] border-solid border-[rgba(255,255,255,0.1)]'>
                  <i className='icofont-envelope-open'></i>
                </span>
                <span className='xl:text-5xl lg:text-4xl md:text-3xl font-light'>
                  demo@example.com
                </span>
              </a>
            </div>
            <div className=' mb-5 footer-single-info'>
              <ul className='social-link flex gap-8'>
                <li>
                  <a
                    href='https://www.example.com'
                    target='_blank'
                    className='sm:w-24 sm:h-24 w-20 h-20 leading-[6rem] block text-white border text-center rounded-[50%] border-solid border-[rgba(255,255,255,0.1)] hover:text-[#252734] hover:border-white hover:bg-white'
                  >
                    <i className='icofont-facebook text-[1.6rem]'></i>
                  </a>
                </li>
                <li>
                  <a
                    href='https://www.example.com'
                    target='_blank'
                    className='sm:w-24 sm:h-24 w-20 h-20 leading-[6rem] block text-white border text-center rounded-[50%] border-solid border-[rgba(255,255,255,0.1)] hover:text-[#252734] hover:border-white hover:bg-white'
                  >
                    <i className='icofont-dribbble text-[1.6rem]'></i>
                  </a>
                </li>
                <li>
                  <a
                    href='https://www.example.com'
                    target='_blank'
                    className='sm:w-24 sm:h-24 w-20 h-20 leading-[6rem] block text-white border text-center rounded-[50%] border-solid border-[rgba(255,255,255,0.1)] hover:text-[#252734] hover:border-white hover:bg-white'
                  >
                    <i className='icofont-linkedin text-[1.6rem]'></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div className='w-full mx-auto max-w-[95%] py-5 border-t-[rgba(255,255,255,0.1)] border-t border-solid'>
        <div className='flex justify-between items-center'>
          <div className='footer-copyright'>
            <p className='copyright-text'>
              © 2021 <a href='index.html'>Milan</a> Made with{' '}
              <i className='icofont-heart'></i> by{' '}
              <a href='https://hasthemes.com/' target='_blank'>
                Milan Bhandari
              </a>{' '}
            </p>
          </div>
          <a href='index.html' className='footer-logo'>
            <div className='logo'>
              <h2>Milan</h2>
            </div>
          </a>
        </div>
      </div>
    </footer>
  );
};
