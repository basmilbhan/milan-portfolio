import React, { useState, useEffect } from 'react';
import SliderComponent from './Slider';

export const Projects = () => {
  const projects = [
    {
      icon: 'interface',
      title: 'UI/UX Design',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
    {
      icon: 'code',
      title: 'Development',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
    {
      icon: 'vector-path',
      title: 'Illustrator',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
    {
      icon: 'ui-browser',
      title: 'SEO',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
  ];

  const project = projects.map((project, key) => (
    <div
      key={key}
      className='project-slide shrink-0  h-full relative transition-transform mr-[50px]'
    >
      <div className='img-box xl:px-40 xl:pt-40 xl:pb-0 xl:h-[45rem] lg:px-20 lg:pt-20 lg:pb-0 lg:h-[35.5rem] overflow-hidden relative z-[1]'>
        <div className='bg-overlay absolute top-0 left-0 w-full h-full bg-[rgba(37,39,52,.95)]'></div>
        <div className='bg-image absolute -translate-x-2/4 -translate-y-2/4 -rotate-45 z-[-1] left-2/4 top-2/4'>
          <img
            src='https://htmldemo.net/lendex/lendex/assets/images/project/project-slider-img-1.jpg'
            alt=''
          />
        </div>
        <div className='image relative z-[1]'>
          <img
            className='w-full'
            src='https://htmldemo.net/lendex/lendex/assets/images/project/project-slider-img-1.jpg'
            alt=''
          />
        </div>
      </div>
      <div className='content'>
        <h4 className='title'>
          <a href='project-details.html'>Givest - Non Profit PSD Template</a>
        </h4>

        <ul className='catagory-nav-item font-light text-2xl relative mt-12'>
          <li>
            <a href=''>Chairty</a>
          </li>
          <li>
            <a href=''>Fund Rising</a>
          </li>
          <li>
            <a href=''>Non Profit</a>
          </li>
        </ul>
      </div>
    </div>
  ));

  return (
    <div className='portfolio relative max-w-[98%] mx-auto px-4 sm:px-6 lg:px-8 mt-8 py-56'>
      <div className='section-content mb-24'>
        <span className='section-tag before:absolute before:top-0 before:left-0 before:content-["||"] text-[1.8rem] font-light relative inline-block pl-11'>
          Awesome Portfolio
        </span>
        <h2 className='section-title'>My Complete Projects</h2>
      </div>
      <SliderComponent
        slides={project}
        arrow={true}
        dot={false}
        slidePerPage={2}
      />
    </div>
  );
};
