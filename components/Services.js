import React from 'react';
import SliderComponent from './Slider';

export const Services = () => {
  const services = [
    {
      icon: 'interface',
      title: 'UI/UX Design',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
    {
      icon: 'code',
      title: 'Development',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
    {
      icon: 'vector-path',
      title: 'Illustrator',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
    {
      icon: 'ui-browser',
      title: 'SEO',
      features: [
        'Landing Pages',
        'User Flow',
        'Wireframing',
        'Prototyping',
        'Mobile App Design',
      ],
    },
  ];

  const Slides = services.map((service, key) => (
    <div
      key={key}
      className='bg-[#252734] p-[7rem] max-w-[35.2rem!important] rounded-md'
    >
      <div className='flex flex-col gap-20 justify-center items-start'>
        <i class={`icofont-${service.icon} text-white  text-7xl`}></i>
        <h4>{service.title}</h4>
        <ul className='flex flex-col  gap-6'>
          {service.features.map((feature, key) => (
            <p key={key} className='flex gap-6 items-center '>
              <i class='icofont-bubble-right text-2xl'></i> {feature}
            </p>
          ))}
        </ul>
      </div>
    </div>
  ));

  return (
    <div className='max-w-[98%] mx-auto px-4 sm:px-6 lg:px-8 mt-8 py-56'>
      <div className='section-content mb-24'>
        <span className='section-tag before:absolute before:top-0 before:left-0 before:content-["||"] text-[1.8rem] font-light relative inline-block pl-11'>
          My Services
        </span>
        <h2 className='section-title'>Service Provide For My Clients.</h2>
      </div>
      <SliderComponent
        slides={Slides}
        arrow={false}
        dot={true}
        slidePerPage={3}
      />
    </div>
  );
};
