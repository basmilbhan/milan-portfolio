import React from 'react';

export const Skills = () => {
  const skills = [
    ['Communication', 'Leadership', 'Teamwork', 'Flexibility'],
    ['70%', ['60%'], ['50%'], ['80%']],
  ];
  return (
    <div className=' bg-[#252734] p-0 m-0'>
      <div className='max-w-[98%] mx-auto px-4 sm:px-6 lg:px-8 mt-8 py-56 flex gap-12'>
        <div className='section-content flex flex-col gap-36 items-start'>
          <div className='flex-col gap-10 flex'>
            <span className='section-tag before:absolute before:top-0 before:left-0 before:content-["||"] text-[1.8rem] font-light relative inline-block pl-11'>
              My Services
            </span>
            <h2 className='section-title'>My Special Skill Field Here.</h2>
          </div>
          <button>
            Get Resume
            <i class='icofont-download'></i>
          </button>
        </div>
        <div className='section-skills flex flex-col gap-20 w-1/2 px-6'>
          {skills[0].map((skill, key) => (
            <div key={key} className='skill-display-wrapper w-full'>
              <div className='skill-progress-single-item p-20 bg-[#2a2c39]'>
                <span className='tag bg-[#2a2c39] inline-block relative z-[2] mb-[25px] text-[1.6rem]'>
                  {skill}
                </span>
                <div className='skill-box w-full h-[5px] relative bg-[rgba(255,255,255,.1)]'>
                  <div
                    style={{ width: skills[1][key] }}
                    className={`progress-line absolute h-full bg-white transition-all duration-[1s] ease-linear left-0 top-0`}
                    data-width={skills[1][key]}
                  >
                    <span className='skill-percentage text-[1.5rem] uppercase absolute right-[-1.5rem] top-[-4.5rem] z-[1]'>
                      {skills[1][key]}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
