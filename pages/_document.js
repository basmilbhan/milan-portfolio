import { Html, Head, Main, NextScript } from 'next/document'

export default function Document() {
  return (
    <Html lang="en">
      <Head />
      <body className='bg-[#2a2c39]'>
        <Main />
        <NextScript />
      </body>
    </Html>
  )
}
