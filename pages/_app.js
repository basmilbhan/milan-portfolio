import '@/styles/globals.css';
import { Iconfont } from '../public/icofont.min.css';

export default function App({ Component, pageProps }) {
  return (
    <>
      <link rel='stylesheet' href={Iconfont} />
      <Component {...pageProps} />
    </>
  );
}
